package com.cpf.sentinel.local;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SentinelLocalApplication {

	public static void main(String[] args) {
		SpringApplication.run(SentinelLocalApplication.class, args);
	}

}