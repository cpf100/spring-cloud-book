package com.cpf.sentinel.local.controller;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.cpf.sentinel.local.config.CustomHandler;
import com.cpf.sentinel.local.service.SentinelLocalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author 马士兵教育:chaopengfei
 * @date 2021/4/19
 */
@RestController
@RequestMapping("/custom-sentinel")
public class CustomController {

	@Autowired
	SentinelLocalService sentinelLocalService;

	@GetMapping("/test-custom")
	@SentinelResource(value = "test-custom",blockHandlerClass = CustomHandler.class,blockHandler = "handleException1")
	public String testLocal(){
		return "正常";
	}

}