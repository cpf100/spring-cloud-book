package com.cpf.sentinel.local.controller;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.alibaba.csp.sentinel.slots.block.BlockException;
import com.cpf.sentinel.local.service.SentinelLocalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author 马士兵教育:chaopengfei
 * @date 2021/4/19
 */
@RestController
@RequestMapping("/sentinel")
public class SentinelController {

	@Autowired
	SentinelLocalService sentinelLocalService;

	@GetMapping("/hello")
	public String hello(){
		return "hello world";
	}

	@GetMapping("/test-local")
	@SentinelResource(value = "test-local",blockHandler = "testLocalFail")
	public String testLocal(@RequestParam String p1){
		return sentinelLocalService.sayHello();
	}

	public String testLocalFail(String p1, BlockException e){
		return "异常了";
	}

	@GetMapping("/test-local2")
	@SentinelResource(fallback = "wo")
	public String testLocal2(){
//		try {
			int a = 1/0;
//		}catch (Exception e){
//			throw new RuntimeException();
//		}

		return "正常test-local2";

	}

	public String wo(Throwable e){
		return "异常了";
	}
}