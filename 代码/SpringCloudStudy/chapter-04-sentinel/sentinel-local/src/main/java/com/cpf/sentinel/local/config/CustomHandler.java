package com.cpf.sentinel.local.config;

import com.alibaba.csp.sentinel.slots.block.BlockException;

/**
 * 自定义流控处理类
 * @author 马士兵教育:chaopengfei
 * @date 2021/4/24
 */
public class CustomHandler {

	public static String handleException1(BlockException e1){

		return "异常1";
	}

	public static String handleException2(BlockException e1){

		return "异常2";
	}
}
