package com.cpf.sentinel.local.service;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import org.springframework.stereotype.Service;

/**
 * @author 马士兵教育:chaopengfei
 * @date 2021/4/19
 */
@Service
public class SentinelLocalService {

	@SentinelResource(value = "sayHello",fallback = "sayHelloFail")
	public String sayHello(){
		System.out.println("hello sentinel");
		return "hello sentinel";
	}

	public String sayHelloFail(){
		System.out.println("hello sentinel fail");
		return "hello sentinel fail";
	}
}
