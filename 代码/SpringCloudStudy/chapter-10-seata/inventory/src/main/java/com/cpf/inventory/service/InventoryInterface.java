package com.cpf.inventory.service;

import io.seata.rm.tcc.api.BusinessActionContext;
import io.seata.rm.tcc.api.LocalTCC;
import io.seata.rm.tcc.api.TwoPhaseBusinessAction;
/**
 * 库存服务TCC服务接口
 * @author 晁鹏飞
 */
@LocalTCC
public interface InventoryInterface {

    @TwoPhaseBusinessAction(name = "inventoryTry" , commitMethod = "inventoryCommit" ,rollbackMethod = "inventoryRollback")
    public String inventoryTry(BusinessActionContext businessActionContext);

    public boolean inventoryCommit(BusinessActionContext businessActionContext);

    public boolean inventoryRollback(BusinessActionContext businessActionContext);
}
