package com.cpf.inventory.entity;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * tbl_inventory实体类
 * @author  晁鹏飞
 */
@Data
public class TblInventory implements Serializable {
    private Integer goodId;

    private Integer num;

    private Date updateTime;

    private static final long serialVersionUID = 1L;
}