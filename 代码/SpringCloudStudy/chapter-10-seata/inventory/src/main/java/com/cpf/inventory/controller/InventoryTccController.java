package com.cpf.inventory.controller;

import com.cpf.inventory.service.InventoryInterface;
import com.cpf.inventory.service.InventoryService;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Seata AT 模式 测试控制层
 * @author 晁鹏飞
 */
@RestController
public class InventoryTccController {


    @Autowired
    private InventoryInterface inventoryInterface;


    @GetMapping("/inventory-tcc")
    @GlobalTransactional(rollbackFor = Exception.class)
    public String twoTcc(){

        inventoryInterface.inventoryTry(null);
//        int i = 1/0;
        return "success";
    }
}
