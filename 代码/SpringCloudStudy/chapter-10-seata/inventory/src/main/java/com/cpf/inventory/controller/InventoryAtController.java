package com.cpf.inventory.controller;

import com.cpf.inventory.service.InventoryService;
import com.cpf.inventory.service.InventoryInterface;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Seata AT 模式 测试控制层
 * @author 晁鹏飞
 */
@RestController
public class InventoryAtController {

    @Autowired
    private InventoryService inventoryService;


    @GetMapping("/reduce")
    public String reduce(Integer goodsId){

        inventoryService.reduce(goodsId);
        return "success";
    }

}
