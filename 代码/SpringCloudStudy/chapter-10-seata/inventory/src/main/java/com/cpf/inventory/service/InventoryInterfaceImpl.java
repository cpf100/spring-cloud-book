package com.cpf.inventory.service;

import io.seata.rm.tcc.api.BusinessActionContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;
/**
 * TCC 库存实现类
 * @author 晁鹏飞
 */
@Component
public class InventoryInterfaceImpl implements InventoryInterface {

    @Override
    @Transactional
    public String inventoryTry(BusinessActionContext businessActionContext) {
        System.out.println("inventory try");
//        System.out.println(1/0);

        return null;
    }

    @Override
    @Transactional
    public boolean inventoryCommit(BusinessActionContext businessActionContext) {
        System.out.println("inventory confirm");
        return true;
    }

    @Override
    @Transactional
    public boolean inventoryRollback(BusinessActionContext businessActionContext) {
        System.out.println("inventory cancel");
        return true;
    }

}
