package com.cpf.inventory.dao;

import com.cpf.inventory.entity.TblInventory;
import org.apache.ibatis.annotations.Mapper;

/**
 * 库存数据库操作类
 * @author 晁鹏飞
 */
@Mapper
public interface TblInventoryDao {
    int deleteByPrimaryKey(Integer goodId);

    int insert(TblInventory record);

    int insertSelective(TblInventory record);

    TblInventory selectByPrimaryKey(Integer goodId);

    int updateByPrimaryKeySelective(TblInventory record);

    int updateByPrimaryKey(TblInventory record);
}