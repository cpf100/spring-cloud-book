package com.cpf.inventory.service;

import java.util.Random;

import com.cpf.inventory.dao.TblInventoryDao;
import com.cpf.inventory.entity.TblInventory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * 库存服务
 * @author 晁鹏飞
 */
@Service
public class InventoryService {
	

	@Autowired
	TblInventoryDao tblInventoryDao;

	@Transactional
	public String reduce(int goodId) {
		TblInventory tblInventory = tblInventoryDao.selectByPrimaryKey(goodId);
		tblInventory.setNum(tblInventory.getNum()-1);

		tblInventoryDao.updateByPrimaryKey(tblInventory);
		
		return "";
	}
	

}