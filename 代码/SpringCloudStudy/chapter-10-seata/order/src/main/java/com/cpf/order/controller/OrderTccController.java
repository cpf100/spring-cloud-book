package com.cpf.order.controller;

import com.cpf.order.service.OrderInterface;
import com.cpf.order.service.OrderService;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Seata测试控制层
 * @author 晁鹏飞
 */
@RestController
public class OrderTccController {

    @Autowired
    private OrderInterface orderInterface;

    @GetMapping("/order-tcc")
    @GlobalTransactional(rollbackFor = Exception.class)
    public String oneTcc() throws InterruptedException {
        orderInterface.orderTry(null);
        return "success";
    }

}
