package com.cpf.order.entity;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * tbl_order实体类
 * @author 晁鹏飞
 */
@Data
public class TblOrder implements Serializable {
    private Integer orderId;

    private Integer goodsId;

    private String buyer;

    private Date updateTime;

}