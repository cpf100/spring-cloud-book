package com.cpf.order.service;

import io.seata.rm.tcc.api.BusinessActionContext;
import io.seata.rm.tcc.api.LocalTCC;
import io.seata.rm.tcc.api.TwoPhaseBusinessAction;

/**
 * 订单服务TCC服务接口
 * @author 晁鹏飞
 */
@LocalTCC
public interface OrderInterface {

    @TwoPhaseBusinessAction(name = "orderTry" , commitMethod = "orderCommit" ,rollbackMethod = "orderRollback")
    public String orderTry(BusinessActionContext businessActionContext);

    public boolean orderCommit(BusinessActionContext businessActionContext);

    public boolean orderRollback(BusinessActionContext businessActionContext);
}
