package com.cpf.order.service;

import com.cpf.order.entity.TblOrder;
import com.cpf.order.dao.TblOrderDao;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;


/**
 * 订单服务
 * @author 晁鹏飞
 */
@Service
public class OrderService {

	@Autowired
	private RestTemplate restTemplate;

	@Autowired
	TblOrderDao tblOrderDao;

//	@GlobalTransactional(rollbackFor = Exception.class)
	@Transactional(rollbackFor = Exception.class)
	public String addOrder(Integer goodsId) {
		restTemplate.getForEntity("http://inventory/reduce?goodsId="+goodsId, null);

		TblOrder tblOrder = new TblOrder();
		tblOrder.setOrderId(1);
		tblOrder.setGoodsId(goodsId);
		tblOrder.setBuyer("晁鹏飞");

		tblOrderDao.insert(tblOrder);

		// 模拟异常
		System.out.println(1/0);
		
		return "";
	}

}
