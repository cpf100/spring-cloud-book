package com.cpf.order.dao;

import com.cpf.order.entity.TblOrder;
import org.apache.ibatis.annotations.Mapper;

/**
 * 订单数据库操作类
 * @author 晁鹏飞
 */
@Mapper
public interface TblOrderDao {
    int deleteByPrimaryKey(Integer orderId);

    int insert(TblOrder record);

    int insertSelective(TblOrder record);

    TblOrder selectByPrimaryKey(Integer orderId);

    int updateByPrimaryKeySelective(TblOrder record);

    int updateByPrimaryKey(TblOrder record);
}