package com.cpf.order.controller;

import com.cpf.order.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Seata AT 模式 测试控制层
 * @author 晁鹏飞
 */
@RestController
public class OrderAtController {

    @Autowired
    OrderService orderService;

    @GetMapping("/order-add")
    public String addOrder(Integer goodsId) throws InterruptedException {
        orderService.addOrder(goodsId);
        return "success";
    }


}
