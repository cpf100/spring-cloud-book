package com.cpf.order.service.impl;

import com.cpf.order.service.OrderInterface;
import io.seata.rm.tcc.api.BusinessActionContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

/**
 * TCC 订单实现类
 * @author 晁鹏飞
 */
@Component
public class OrderInterfaceImpl implements OrderInterface {

    /**
     * Try 方法
     * @param businessActionContext
     * @return
     */
    @Override
    @Transactional
    public String orderTry(BusinessActionContext businessActionContext) {
        System.out.println("order try");

        inventoryTcc();
        System.out.println(1/0);
        return null;
    }

    /**
     * Commit方法
     * @param businessActionContext
     * @return
     */
    @Override
    @Transactional
    public boolean orderCommit(BusinessActionContext businessActionContext) {
        System.out.println("order confirm");
        return true;
    }

    /**
     * Cancel方法
     * @param businessActionContext
     * @return
     */
    @Override
    @Transactional
    public boolean orderRollback(BusinessActionContext businessActionContext) {
        System.out.println("order cancel");
        return true;
    }


    @Autowired
    private RestTemplate restTemplate;

    private void inventoryTcc() {
        restTemplate.getForEntity("http://inventory/inventory-tcc", null);
    }

}
