package com.cpf.pay.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author 马士兵教育:chaopengfei
 * @date 2021/3/9
 */
@RestController
@RequestMapping("/provider")
public class ProviderController {


	@GetMapping("/test")
	public String test() {
		System.out.println(port);
		return "pay restful provider";
	}

	@Value("${server.port}")
	private String port;

	@GetMapping("/test-ribbon")
	public String testRibbon() {
		System.out.println(port);
		return "pay restful provider";
	}
}