package com.cpf.authcodeserver.util;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * @author 晁鹏飞
 * @date 2021/4/19
 */
public class JwtUtil {
	// 只在服务端存储的密钥
	private static final String secret = "asdfasdf";

	public static String createToken(String subject){

		String token = Jwts.builder().setSubject(subject)
				.setExpiration(new Date(System.currentTimeMillis() + 1000 * 60 * 60))
				.signWith(SignatureAlgorithm.HS256, secret)
				.compact();

		return token;
	}

	public static String parseToken(String token){
		Claims body = Jwts.parser().setSigningKey(secret).parseClaimsJws(token).getBody();

		String subject = body.getSubject();
		return subject;
	}

	public static void main(String[] args) throws InterruptedException {
		String name = "教育";

		String token = createToken(name);
		System.out.println("生成的token:"+token);

		String srcStr = parseToken(token);
		System.out.println("解析出来值："+srcStr);

	}
}
