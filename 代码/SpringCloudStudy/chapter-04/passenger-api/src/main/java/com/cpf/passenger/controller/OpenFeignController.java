package com.cpf.passenger.controller;

import com.cpf.passenger.feign.PayServiceClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.circuitbreaker.CircuitBreakerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author 马士兵教育:chaopengfei
 * @date 2021/3/9
 */
@RestController
@RequestMapping("/openfeign")
public class OpenFeignController {

	@Autowired
	private PayServiceClient payServiceClient;

	@GetMapping("/test")
	public String testFeign(){
		return payServiceClient.restfulTest();
	}

//	@Autowired
//	private CircuitBreakerFactory circuitBreakerFactory;
//
//	@GetMapping("/test-resilience4j")
//	public String testResilience4j(){
//		return circuitBreakerFactory.create("testFeign").run(()->payServiceClient.restfulTest(),throwable -> "熔断返回值");
//
//	}

}
