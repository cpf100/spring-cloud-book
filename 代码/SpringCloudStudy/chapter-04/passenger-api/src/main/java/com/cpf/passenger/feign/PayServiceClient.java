package com.cpf.passenger.feign;

import com.cpf.passenger.config.AuthConfiguration;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * @author 马士兵教育:chaopengfei
 * @date 2021/3/13
 */
// 此处写要调用的服务名
@FeignClient(name = "my-pay-service",url = "http://localhost:8092/",configuration = AuthConfiguration.class)
public interface PayServiceClient {

	/**
	 * 此处写要调用服务的路径，和调用的方法
 	 */
	@RequestMapping(method = RequestMethod.GET, value = "/provider/test")
	String restfulTest();
}
