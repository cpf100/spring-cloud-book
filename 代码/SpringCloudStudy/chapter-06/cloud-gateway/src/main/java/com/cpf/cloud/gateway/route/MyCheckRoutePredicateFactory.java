package com.cpf.cloud.gateway.route;

import org.springframework.cloud.gateway.handler.predicate.AbstractRoutePredicateFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;

import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;

/**
 * @author 马士兵教育:chaopengfei
 * @date 2021/3/22
 */
//@Component
public class MyCheckRoutePredicateFactory extends AbstractRoutePredicateFactory<MyCheckRoutePredicateFactory.MyConfig> {
	public MyCheckRoutePredicateFactory() {
		super(MyConfig.class);
	}

	@Override
	public List<String> shortcutFieldOrder() {
		return Arrays.asList(new String[] {"name"});
	}


	@Override
	public Predicate<ServerWebExchange> apply(MyConfig config) {
		return (ServerWebExchange serverWebExchange) -> {
			System.out.println("自定义路由断言");
			String name = serverWebExchange.getRequest().getQueryParams().getFirst("name");
			if (name.equals(config.getName())){
				return true;
			}else {
				return false;
			}
		};
	}

	public static class MyConfig{
		private String name;

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}
	}
}
