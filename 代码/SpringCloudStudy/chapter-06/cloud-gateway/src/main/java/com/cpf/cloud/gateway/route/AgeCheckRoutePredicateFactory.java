package com.cpf.cloud.gateway.route;

import org.springframework.cloud.gateway.handler.predicate.AbstractRoutePredicateFactory;
import org.springframework.web.server.ServerWebExchange;

import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;

/**
 * @author 马士兵教育:chaopengfei
 * @date 2021/3/22
 */
//@Component
public class AgeCheckRoutePredicateFactory extends AbstractRoutePredicateFactory<AgeCheckRoutePredicateFactory.MyConfig> {
	public AgeCheckRoutePredicateFactory() {
		super(MyConfig.class);
	}

	@Override
	public List<String> shortcutFieldOrder() {
		return Arrays.asList(new String[] {"minAge","maxAge"});
	}


	@Override
	public Predicate<ServerWebExchange> apply(MyConfig config) {
		return (ServerWebExchange serverWebExchange) -> {
			// TODO 获取请求参数age，判断是否满足[18, 60)
			String age = serverWebExchange.getRequest().getQueryParams().getFirst("age");


			if (!age.matches("[0-9]+")) {
				return false;
			}

			int iAge = Integer.parseInt(age);

			if (iAge >= config.minAge && iAge < config.maxAge) {
				return true;
			} else {
				return false;
			}

		};
	}

	public static class MyConfig{
		private int minAge;
		private int maxAge;

		public int getMinAge() {
			return minAge;
		}

		public void setMinAge(int minAge) {
			this.minAge = minAge;
		}

		public int getMaxAge() {
			return maxAge;
		}

		public void setMaxAge(int maxAge) {
			this.maxAge = maxAge;
		}
	}
}
