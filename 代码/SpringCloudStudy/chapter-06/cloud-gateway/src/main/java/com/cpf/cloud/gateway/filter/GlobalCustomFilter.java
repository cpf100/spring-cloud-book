package com.cpf.cloud.gateway.filter;

import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

/**
 * @author 马士兵教育:chaopengfei
 * @date 2021/3/25
 */
@Configuration
public class GlobalCustomFilter{

	@Bean
	@Order(-1)
	public GlobalFilter zero(){
		return (exchange,chain)->{
			return chain.filter(exchange)
					.then(Mono.fromRunnable(()-> { System.out.println("-1号过滤器");}));
		};
	}

	@Bean
	@Order(0)
	public GlobalFilter second(){
		return (exchange,chain)->{
			return chain.filter(exchange).then(Mono.fromRunnable(()-> System.out.println("0号过滤器")));
		};
	}

	@Bean
	@Order(1)
	public GlobalFilter first(){
		return (exchange,chain)->{
			return chain.filter(exchange).then(Mono.fromRunnable(()-> System.out.println("1号过滤器")));
		};
	}
}
