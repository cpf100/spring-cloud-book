package com.cpf.passenger.feign;

/**
 * @author 马士兵教育:chaopengfei
 * @date 2021/3/14
 */
public class PayServiceClientFallback implements PayServiceClient {
	@Override
	public String restfulTest() {
		return "feign熔断了";
	}
}
