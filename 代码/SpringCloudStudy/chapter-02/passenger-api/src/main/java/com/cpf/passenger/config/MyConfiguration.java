package com.cpf.passenger.config;

import org.springframework.cloud.loadbalancer.annotation.LoadBalancerClient;

/**
 * @author 马士兵教育:chaopengfei
 * @date 2021/3/16
 */
@LoadBalancerClient(name = "pay-service",configuration = CustomLoadBalancerConfiguration.class)
public class MyConfiguration {
}
