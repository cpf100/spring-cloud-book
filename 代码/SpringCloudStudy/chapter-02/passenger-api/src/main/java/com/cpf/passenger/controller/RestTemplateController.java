package com.cpf.passenger.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

/**
 * @author 马士兵教育:chaopengfei
 * @date 2021/3/9
 */
@RestController
@RequestMapping("/restful")
public class RestTemplateController {

	@Autowired
	private RestTemplate restTemplate;

	@GetMapping("/test")
	public String test() {
		// 此处注意：我们一直写的ip和port，现在是服务名（其实本质是虚拟主机名）:pay-service
		String url = "http://pay-service/provider/test-ribbon";
		return restTemplate.getForObject(url, String.class);

	}

//	@Autowired
//	private CircuitBreakerFactory circuitBreakerFactory;

//	@GetMapping("/test-resilience4j")
//	public String testResilience4j(){
//		String url = "http://pay-service/provider/test";
//		return circuitBreakerFactory.create("testHystrix").run(()->restTemplate.getForObject(url, String.class),throwable -> "熔断返回值");
//
//	}

}
