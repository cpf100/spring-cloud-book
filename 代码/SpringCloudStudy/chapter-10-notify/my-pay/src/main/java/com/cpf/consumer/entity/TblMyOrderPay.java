package com.cpf.consumer.entity;

import java.io.Serializable;
import lombok.Data;

/**
 * tbl_my_order_pay
 * @author 
 */
@Data
public class TblMyOrderPay implements Serializable {
    /**
     * id
     */
    private Integer id;

    /**
     * 订单Id
     */
    private Integer orderId;

    /**
     * 订单支付状态：1：未支付，2：已支付
     */
    private Integer orderStatus;

    private static final long serialVersionUID = 1L;
}