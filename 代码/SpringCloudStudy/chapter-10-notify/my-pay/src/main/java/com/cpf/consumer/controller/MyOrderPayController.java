package com.cpf.consumer.controller;

import com.cpf.consumer.entity.TblMyOrderPay;
import com.cpf.consumer.service.MyOrderPayService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 支付结果回调控制类
 * @author 马士兵教育:chaopengfei
 * @date 2021/4/13
 */
@RestController
@RequestMapping
public class MyOrderPayController {

	@Autowired
	private MyOrderPayService myOrderPayService;

	/**
	 * 支付成功回调接口
	 * @param pay
	 * @return
	 */
	@PostMapping("/my-pay")
	public String myPay(@RequestBody TblMyOrderPay pay){

		myOrderPayService.update(pay);

		return "成功";
	}
}