package com.cpf.consumer.service;

import com.cpf.consumer.entity.TblMyOrderPay;

/**
 * MyOrderPay接口层
 * @author 马士兵教育:chaopengfei
 * @date 2021/4/13
 */
public interface MyOrderPayService {

	public int update(TblMyOrderPay orderPay);
}
