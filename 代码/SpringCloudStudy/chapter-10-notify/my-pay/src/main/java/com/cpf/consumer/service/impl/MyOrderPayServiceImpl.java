package com.cpf.consumer.service.impl;

import com.cpf.consumer.dao.TblMyOrderPayDao;
import com.cpf.consumer.entity.TblMyOrderPay;
import com.cpf.consumer.service.MyOrderPayService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * MyOrderPay服务实现类
 * @author 马士兵教育:chaopengfei
 * @date 2021/4/13
 */
@Service
public class MyOrderPayServiceImpl implements MyOrderPayService {

	@Autowired
	TblMyOrderPayDao myOrderPayDao;

	@Override
	public int update(TblMyOrderPay orderPay) {
		return myOrderPayDao.updateByOrderId(orderPay);
	}
}
