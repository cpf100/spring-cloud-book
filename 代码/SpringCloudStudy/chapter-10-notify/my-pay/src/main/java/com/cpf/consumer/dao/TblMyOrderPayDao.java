package com.cpf.consumer.dao;

import com.cpf.consumer.entity.TblMyOrderPay;
import org.apache.ibatis.annotations.Mapper;

/**
 * TblMyOrderPay 数据操作层
 * @author 晁鹏飞
 */
@Mapper
public interface TblMyOrderPayDao {

    int deleteByPrimaryKey(Integer id);

    int insert(TblMyOrderPay record);

    int insertSelective(TblMyOrderPay record);

    TblMyOrderPay selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(TblMyOrderPay record);

    int updateByOrderId(TblMyOrderPay record);

    int updateByPrimaryKey(TblMyOrderPay record);
}