package com.cpf.producer.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * transaction_log
 * @author 晁鹏飞
 */
@Data
public class TransactionLog implements Serializable {
    /**
     * 事务ID
     */
    private String id;

    /**
     * 订单业务号
     */
    private String business;

    /**
     * 对应业务表中的主键
     */
    private String foreignKey;

}