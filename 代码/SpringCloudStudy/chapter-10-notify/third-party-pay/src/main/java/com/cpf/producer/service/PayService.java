package com.cpf.producer.service;

import com.cpf.producer.entity.TblOrderPay;
import org.apache.rocketmq.client.exception.MQClientException;

/**
 * @author 马士兵教育:chaopengfei
 * @date 2021/4/12
 */
public interface PayService {

	public void pay(TblOrderPay tblOrderPay,String topic) throws MQClientException;

	public int insert(TblOrderPay tblOrderPay , String transactionId);

	public TblOrderPay queryByOrderId(Integer orderId);
}
