package com.cpf.producer.dao;

import com.cpf.producer.entity.TransactionLog;
import org.apache.ibatis.annotations.Mapper;

/**
 * TransactionLog 数据操作层
 * @author 晁鹏飞
 */
@Mapper
public interface TransactionLogDao {

    int deleteByPrimaryKey(String id);

    int insert(TransactionLog record);

    int insertSelective(TransactionLog record);

    TransactionLog selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(TransactionLog record);

    int updateByPrimaryKey(TransactionLog record);

    int selectCount(String id);
}