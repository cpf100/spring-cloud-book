package com.cpf.producer.config;

import com.alibaba.fastjson.JSONObject;
import com.cpf.producer.dao.TransactionLogDao;
import com.cpf.producer.entity.TblOrderPay;
import com.cpf.producer.service.PayService;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.client.producer.LocalTransactionState;
import org.apache.rocketmq.client.producer.TransactionListener;
import org.apache.rocketmq.common.message.Message;
import org.apache.rocketmq.common.message.MessageExt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 事务监听器
 * @author 晁鹏飞
 */
@Component
@Slf4j
public class OrderTransactionListener implements TransactionListener {

    @Autowired
    PayService payService;

    @Autowired
    TransactionLogDao transactionLogDao;

    /**
     * 发送half msg 返回send ok后调用的方法
     * @param message
     * @param o
     * @return
     */
    @Override
    public LocalTransactionState executeLocalTransaction(Message message, Object o) {
        log.info("开始执行本地事务....");
        LocalTransactionState state;
        String keys = message.getKeys();
        System.out.println("keys:"+keys);
        try{
            String body = new String(message.getBody());
            TblOrderPay order = JSONObject.parseObject(body, TblOrderPay.class);
            payService.insert(order,message.getTransactionId());

            state = LocalTransactionState.COMMIT_MESSAGE;
//            state = LocalTransactionState.UNKNOW;
            log.info("本地事务已提交。{}",message.getTransactionId());

        }catch (Exception e){
            log.info("执行本地事务失败。{}",e);
            state = LocalTransactionState.ROLLBACK_MESSAGE;
        }
        return state;
    }

    /**
     * 回查 走的方法
     * @param messageExt
     * @return
     */
    @Override
    public LocalTransactionState checkLocalTransaction(MessageExt messageExt) {

        // 回查多次失败 人工补偿。提醒人。发邮件的。
        log.info("开始回查本地事务状态。{}",messageExt.getTransactionId());
        LocalTransactionState state;
        String transactionId = messageExt.getTransactionId();
        if (transactionLogDao.selectCount(transactionId)>0){
            state = LocalTransactionState.COMMIT_MESSAGE;
        }else {
            state = LocalTransactionState.ROLLBACK_MESSAGE;
        }
        System.out.println();
        log.info("结束本地事务状态查询：{}",state);
        return state;
    }
}