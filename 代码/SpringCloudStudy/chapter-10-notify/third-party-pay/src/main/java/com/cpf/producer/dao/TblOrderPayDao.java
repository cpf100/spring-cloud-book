package com.cpf.producer.dao;

import com.cpf.producer.entity.TblOrderPay;
import org.apache.ibatis.annotations.Mapper;

/**
 * TblOrderPay 数据操作层
 * @author
 */
@Mapper
public interface TblOrderPayDao {

    int deleteByPrimaryKey(Integer id);

    int insert(TblOrderPay record);

    int insertSelective(TblOrderPay record);

    TblOrderPay selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(TblOrderPay record);

    int updateByPrimaryKey(TblOrderPay record);

    TblOrderPay selectByOrderId(Integer orderId);
}