package com.cpf.producer.controller;

import com.cpf.producer.config.TransactionProducer;
import com.cpf.producer.entity.TblOrderPay;
import com.cpf.producer.service.PayService;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.client.exception.MQClientException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 事务消息Controller
 * @author 晁鹏飞
 */
@RestController
@Slf4j
public class PayController {

    @Autowired
    PayService payService;

    /**
     * 支付业务接口
     * @param orderPay
     * @throws MQClientException
     */
    @PostMapping("/pay")
    public void pay(@RequestBody TblOrderPay orderPay) throws MQClientException {
        payService.pay(orderPay,"pay-order");
    }

    /**
     * 支付结果查询接口
     * @param orderId
     * @return
     * @throws MQClientException
     */
    @GetMapping("/pay-query/{orderId}")
    public TblOrderPay payQuery(@PathVariable("orderId") Integer orderId) throws MQClientException {
        return payService.queryByOrderId(orderId);

    }


}