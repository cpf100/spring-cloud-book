package com.cpf.producer.service.impl;

import com.alibaba.fastjson.JSON;
import com.cpf.producer.config.TransactionProducer;
import com.cpf.producer.dao.TblOrderPayDao;
import com.cpf.producer.dao.TransactionLogDao;
import com.cpf.producer.entity.TblOrderPay;
import com.cpf.producer.entity.TransactionLog;
import com.cpf.producer.service.PayService;
import org.apache.rocketmq.client.exception.MQClientException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author 马士兵教育:chaopengfei
 * @date 2021/4/12
 */
@Service
public class PayServiceImpl implements PayService {

	@Autowired
	TblOrderPayDao tblOrderPayDao;

	@Autowired
	TransactionLogDao transactionLogDao;

	@Autowired
	TransactionProducer transactionProducer;

	@Override
	public void pay(TblOrderPay tblOrderPay,String topic) throws MQClientException {
		transactionProducer.send(JSON.toJSONString(tblOrderPay),topic);
	}

	@Transactional
	@Override
	public int insert(TblOrderPay tblOrderPay , String transactionId) {
		// 写入事务日志
		TransactionLog log = new TransactionLog();
		log.setId(transactionId);
		log.setBusiness(tblOrderPay.getOrderId()+"");
		log.setForeignKey(tblOrderPay.getId()+"");
		transactionLogDao.insert(log);

		return tblOrderPayDao.insertSelective(tblOrderPay);
	}

	@Override
	public TblOrderPay queryByOrderId(Integer orderId) {
		return tblOrderPayDao.selectByOrderId(orderId);
	}
}