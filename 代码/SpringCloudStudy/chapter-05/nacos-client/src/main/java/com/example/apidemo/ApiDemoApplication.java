package com.example.apidemo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
@RefreshScope
public class ApiDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(ApiDemoApplication.class, args);
    }

    @Value("${name}")
    private String name;

    @GetMapping("/test-nacos-client")
    public String testNacosClient(){

        return "name:"+name;
    }


    @PatchMapping("/api")
    public String patch(@RequestBody ApiBean bean){
        System.out.println(bean.getAge());
        System.out.println(bean.getName());

        return "patch成功"+name;
    }

    @GetMapping("/api-get")
    public String patch(){
        String name = applicationContext.getEnvironment().getProperty("name");
        return name;
    }

    @Autowired
    private ConfigurableApplicationContext applicationContext;
    @GetMapping("/api-get2")
    public String patch3(){
		String cpf1 = applicationContext.getEnvironment().getProperty("cpf1");
        String cpf2 = applicationContext.getEnvironment().getProperty("cpf2");
		return cpf1+" "+cpf2;
    }

}
