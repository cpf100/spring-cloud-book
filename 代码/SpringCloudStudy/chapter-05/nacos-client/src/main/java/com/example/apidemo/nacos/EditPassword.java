package com.example.apidemo.nacos;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 * @author 马士兵教育:chaopengfei
 * @date 2020/12/17
 */
public class EditPassword {
	public static void main(String[] args) {

		String encode = new BCryptPasswordEncoder().encode("123");
		System.out.println(encode);

	}
}
