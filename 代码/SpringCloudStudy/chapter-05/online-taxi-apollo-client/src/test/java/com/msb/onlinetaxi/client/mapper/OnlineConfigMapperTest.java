package com.msb.onlinetaxi.client.mapper;

import com.msb.onlinetaxi.client.entity.OnlineConfig;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * @author 马士兵教育:chaopengfei
 * @date 2021/1/21
 */
@SpringBootTest
public class OnlineConfigMapperTest {

	@Autowired
	OnlineConfigMapper onlineConfigMapper;

	@Test
	public void insert(){

		OnlineConfig entity = new OnlineConfig();
		entity.setAge(12);
		entity.setName("name");

		onlineConfigMapper.insert(entity);
	}
}
