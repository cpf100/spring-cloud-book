package com.msb.onlinetaxi.client.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.msb.onlinetaxi.client.entity.OnlineConfig;
import org.springframework.stereotype.Repository;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 晁鹏飞
 * @since 2021-01-21
 */
@Repository
public interface OnlineConfigMapper extends BaseMapper<OnlineConfig> {

	OnlineConfig selectByName(String name);

}
