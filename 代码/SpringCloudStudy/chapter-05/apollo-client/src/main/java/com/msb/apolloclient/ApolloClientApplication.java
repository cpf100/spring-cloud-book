package com.msb.apolloclient;

import com.ctrip.framework.apollo.spring.annotation.EnableApolloConfig;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@EnableApolloConfig
@RestController
public class ApolloClientApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApolloClientApplication.class, args);
	}


	@GetMapping("/config")
	public String getConfig(@Value("${name}") String msbEnable){

		return msbEnable;
	}

}
