package com.msb.apolloclient;

import com.ctrip.framework.apollo.Config;
import com.ctrip.framework.apollo.ConfigService;

import java.util.concurrent.TimeUnit;

/**
 * @author 马士兵教育:chaopengfei
 * @date 2021/1/13
 */
public class ConfigGetMain {
	public static void main(String[] args) throws InterruptedException {
		// 读取默认的namespace：application
//		Config config = ConfigService.getAppConfig();
		// namespace
		Config config = ConfigService.getConfig("jdbcnamespace");

		while (true){
			TimeUnit.SECONDS.sleep(1);
			// 取不到值，默认是wo
			String wo = config.getProperty("jdbc", "jdbc");

			System.out.println(wo);
		}

	}
}
