package com.cpf.config.client.controller;

//import com.online.taxi.service.ConfigService;
//import com.online.taxi.component.GitConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
/**测试获取属性的类
 * @author yueyi2019
 */
@RefreshScope
@RestController
@RequestMapping("/config")
public class ConfigController {

	@Value("${testKey}")
	private String testKey;

	@GetMapping("/testKey")
	public String testKey(){
		return testKey;
	}

}