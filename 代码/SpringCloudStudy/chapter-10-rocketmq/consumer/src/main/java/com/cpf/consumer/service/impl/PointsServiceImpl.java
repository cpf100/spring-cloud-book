package com.cpf.consumer.service.impl;

import com.cpf.consumer.dao.TblPointsDao;
import com.cpf.consumer.entity.OrderBase;
import com.cpf.consumer.entity.TblPoints;
import com.cpf.consumer.service.PointsService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 积分接口实现类
 * @author 晁鹏飞
 */
@Service
@Slf4j
public class PointsServiceImpl implements PointsService {

	@Autowired
	TblPointsDao pointsMapper;

	@Override
	public void increasePoints(OrderBase order) {


		TblPoints points = new TblPoints();
		points.setUserId(1L);
		points.setOrderNo(order.getOrderNo());
		points.setPoints(10);
		points.setRemarks("商品消费共10元，获得积分" + points.getPoints());
		pointsMapper.insert(points);
		log.info("已为订单号码{}增加积分。", points.getOrderNo());

	}
}