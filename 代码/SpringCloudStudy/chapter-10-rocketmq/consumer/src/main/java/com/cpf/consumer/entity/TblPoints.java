package com.cpf.consumer.entity;

import java.io.Serializable;
import lombok.Data;

/**
 * tbl_points
 * @author 晁鹏飞
 */
@Data
public class TblPoints implements Serializable {
    /**
     * 主键
     */
    private Long id;

    /**
     * 用户id
     */
    private Long userId;

    /**
     * 订单编号
     */
    private String orderNo;

    /**
     * 积分
     */
    private Integer points;

    /**
     * 备注
     */
    private String remarks;

}