package com.cpf.consumer.service;

import com.cpf.consumer.entity.OrderBase;

/**
 * 积分接口
 * @author 晁鹏飞
 */
public interface PointsService {

    public void increasePoints(OrderBase order);
}