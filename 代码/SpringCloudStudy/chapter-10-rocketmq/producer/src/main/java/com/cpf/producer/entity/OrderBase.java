package com.cpf.producer.entity;

import java.io.Serializable;
import lombok.Data;

/**
 * order_base
 * @author 晁鹏飞
 */
@Data
public class OrderBase implements Serializable {
    private Integer id;

    private String orderNo;

}