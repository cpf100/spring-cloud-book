package com.cpf.producer.dao;

import com.cpf.producer.entity.OrderBase;
import org.apache.ibatis.annotations.Mapper;

/**
 * OrderBase 数据操作层
 */
@Mapper
public interface OrderBaseDao {
    int deleteByPrimaryKey(Integer id);

    int insert(OrderBase record);

    int insertSelective(OrderBase record);

    OrderBase selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(OrderBase record);

    int updateByPrimaryKey(OrderBase record);
}