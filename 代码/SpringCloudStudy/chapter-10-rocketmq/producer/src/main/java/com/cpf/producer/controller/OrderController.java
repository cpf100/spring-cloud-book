package com.cpf.producer.controller;

import com.cpf.producer.config.TransactionProducer;
import com.cpf.producer.entity.OrderBase;
import com.cpf.producer.service.OrderService;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.client.exception.MQClientException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 事务消息Controller
 * @author 晁鹏飞
 */
@RestController
@Slf4j
@RequestMapping("/order")
public class OrderController {

    @Autowired
    OrderService orderService;

    @PostMapping("/create")
    public void createOrder(@RequestBody OrderBase order) throws MQClientException {
        orderService.createOrder(order);
    }

    @Autowired
    TransactionProducer producer;

    @PostMapping("/create-send")
    public void createOrderSend(@RequestBody OrderBase order) throws MQClientException {
        producer.send("wo","cpf");
    }
}