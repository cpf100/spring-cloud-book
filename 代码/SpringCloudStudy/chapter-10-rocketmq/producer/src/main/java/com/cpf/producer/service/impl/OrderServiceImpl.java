package com.cpf.producer.service.impl;

import com.alibaba.fastjson.JSON;
import com.cpf.producer.config.TransactionProducer;
import com.cpf.producer.dao.TransactionLogDao;
import com.cpf.producer.entity.TransactionLog;
import com.cpf.producer.service.OrderService;
import com.cpf.producer.dao.OrderBaseDao;
import com.cpf.producer.dao.TransactionLogDao;
import com.cpf.producer.entity.OrderBase;
import com.cpf.producer.entity.TransactionLog;
import com.cpf.producer.service.OrderService;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.client.exception.MQClientException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.lang.reflect.InvocationTargetException;
import java.util.concurrent.TimeUnit;

/**
 * OrderService的实现类
 * @author 晁鹏飞
 */
@Service
@Slf4j
public class OrderServiceImpl implements OrderService {
    @Autowired
    OrderBaseDao orderMapper;
    @Autowired
    TransactionLogDao transactionLogMapper;
    @Autowired
    TransactionProducer producer;

    /**
     * 执行本地事务时调用，将订单数据和事务日志写入本地数据库
     * @param order
     * @param transactionId
     * @throws InvocationTargetException
     * @throws IllegalAccessException
     */
    @Transactional
    @Override
    public void createOrder(OrderBase order, String transactionId) throws InvocationTargetException, IllegalAccessException {

        //1.创建订单
        orderMapper.insert(order);

        //2.写入事务日志
        TransactionLog log = new TransactionLog();
        log.setId(transactionId);
        log.setBusiness(order.getOrderNo());
        log.setForeignKey(String.valueOf(order.getId()));
        transactionLogMapper.insert(log);

        // 在操作数据库的时候增加一个除0异常
        System.out.println(1/0);
//        try {
//            TimeUnit.MINUTES.sleep(5);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }

    }

    /**
     * controller调用，只用于向RocketMQ发送事务消息
     * @param order
     * @throws MQClientException
     */
    @Override
    public void createOrder(OrderBase order) throws MQClientException {
        // 向RocketMQ发送消息，topic为：order-create
        producer.send(JSON.toJSONString(order),"order-create");
    }
}