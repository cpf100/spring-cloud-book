package com.cpf.producer.service;

import com.cpf.producer.entity.OrderBase;
import org.apache.rocketmq.client.exception.MQClientException;

import java.lang.reflect.InvocationTargetException;

/**
 *  订单操作接口
 * @author  晁鹏飞
 */
public interface OrderService {
    /**
     * 创建订单，实际接口
     * @param order
     * @param transactionId
     * @throws InvocationTargetException
     * @throws IllegalAccessException
     */
    public void createOrder(OrderBase order, String transactionId) throws InvocationTargetException, IllegalAccessException;

    /**
     * 创建订单，发送消息接口
     * @param order
     * @throws MQClientException
     */
    public void createOrder(OrderBase order) throws MQClientException;
}