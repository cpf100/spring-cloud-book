package com.cpf.pay.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * @author 马士兵教育:chaopengfei
 * @date 2021/3/9
 */
@RestController
@RequestMapping("/provider")
@Slf4j
public class ProviderController {


	@GetMapping("/test")
	public String test(HttpServletRequest request) {
		log.info("X-B3-TraceId:"+request.getHeader("X-B3-TraceId"));
		log.info("X-B3-SpanId:"+request.getHeader("X-B3-SpanId"));
		log.info("X-B3-ParentSpanId:"+request.getHeader("X-B3-ParentSpanId"));

		int i = 1/0;
		return "pay restful provider";
	}

	@Value("${server.port}")
	private String port;

	@GetMapping("/test-ribbon")
	public String testRibbon() {
		System.out.println(port);
		return "pay restful provider";
	}



}