package com.cpf.passenger.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * @author 马士兵教育:chaopengfei
 * @date 2021/3/22
 */
@RestController
@RequestMapping("/gateway")
public class GatewayController {

	@GetMapping("/hello")
	public String hello(){
		return "hello gateway I'm passenger-api";
	}

	@GetMapping("/hello/1")
	public String hello1(){
		return "hello gateway I'm passenger-api path:1";
	}


	@GetMapping("/add-request")
	public String addRequest(HttpServletRequest request){
		String header = request.getHeader("x-header-arg");
		return header;
	}
}
