package com.cpf.cloud.admin.server.ding;

import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.springframework.http.HttpStatus;

import java.util.HashMap;

/**
 * 钉钉发送工具类
 */
@Slf4j
public class DingtalkUtils {

    public static void main(String[] args) {
        pushInfoToDingding("测试消息通知", "30709c7b3f38101b853d1c50a211031d2ad358c1dca40e57d1c6f8435938f56c");
    }

    public static Boolean pushInfoToDingding(String textMsg, String dingURL) {

        HashMap<String, Object> resultMap = new HashMap<>(8);
        resultMap.put("msgtype", "text");

        HashMap<String, String> textItems = new HashMap<>(8);
        textItems.put("content", textMsg);
        resultMap.put("text", textItems);

        HashMap<String, Object> atItems = new HashMap<>(8);
        atItems.put("atMobiles", null);
        atItems.put("isAtAll", false);
        resultMap.put("at", atItems);

        
        dingURL = "https://oapi.dingtalk.com/robot/send?access_token=" + dingURL;
        try {
            HttpClient httpClient = HttpClients.createDefault();
            StringEntity stringEntity = new StringEntity(JSON.toJSONString(resultMap), "utf-8");

            HttpPost httpPost = createConnectivity(dingURL);
            httpPost.setEntity(stringEntity);
            HttpResponse response = httpClient.execute(httpPost);
            if (response.getStatusLine().getStatusCode() == HttpStatus.OK.value()) {
                String result = EntityUtils.toString(response.getEntity(), "utf-8");
                System.out.println(result);
                log.info("执行结果：{}" , result);
            }
            return Boolean.TRUE;
        } catch (Exception e) {
            e.printStackTrace();
            return Boolean.FALSE;
        }
    }


    static HttpPost createConnectivity(String restUrl) {
        HttpPost post = new HttpPost(restUrl);
        post.setHeader("Content-Type", "application/json");
        post.setHeader("Accept", "application/json");
        post.setHeader("X-Stream", "true");
        return post;
    }
}