package com.online.taxi.order.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

/**
 * 接口层
 */
@RestController
@RequestMapping("/grab")
public class GrabOrderController {

    @Autowired
    private RestTemplate restTemplate;
    
    @GetMapping("/do/{orderId}")
    public String grabMysql(@PathVariable("orderId") int orderId, int userId){

        // 电商秒杀
        String url = "http://service-order" + "/seckill/do/"+orderId+"?userId="+userId;

        restTemplate.getForEntity(url, String.class).getBody();
        return "成功";
    }

}